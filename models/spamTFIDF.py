import pandas as pd
import numpy as np
import re
from sklearn import metrics
from sklearn.utils import shuffle
from sklearn.naive_bayes import BernoulliNB, MultinomialNB, GaussianNB

SPAM_LABEL = np.int64(1)
HAM_LABEL = np.int64(0)

def readData():
    email_dataset = shuffle(pd.read_csv('../datasets/emails.csv', usecols=[0, 1], encoding='latin-1')[1:])
    email_dataset.columns = ['content', 'label']
    n = int(email_dataset.shape[0])

    return email_dataset.iloc[:int(n / 2)], email_dataset.iloc[int(n / 2):]


def generate_tabu_list(path, train_set, tabu_size=200, ignore=3):
    spam_TF_dict = {}
    ham_TF_dict = {}
    IDF_dict = {}

    for i in range(train_set.shape[0]):
        finds = re.findall('[A-Za-z]+', train_set.iloc[i].content)
        if train_set.iloc[i].label == SPAM_LABEL:
            # print(train_set.iloc[i].content, type(train_set.iloc[i].label), sep="\n")
            for find in finds:
                if len(find) < ignore:
                    continue
                find = find.lower()
                try:
                    spam_TF_dict[find] += 1
                except:
                    spam_TF_dict[find] = spam_TF_dict.get(find, 1)
                    ham_TF_dict[find] = ham_TF_dict.get(find, 0)
        else:
            for find in finds:
                if len(find) < ignore: continue
                find = find.lower()
                try:
                    ham_TF_dict[find] += 1
                except:
                    spam_TF_dict[find] = spam_TF_dict.get(find, 0)
                    ham_TF_dict[find] = ham_TF_dict.get(find, 1)

        word_set = set()
        for find in finds:
            if len(find) < ignore:
                continue
            find = find.lower()
            if not (find in word_set):
                try:
                    IDF_dict[find] += 1
                except:
                    IDF_dict[find] = IDF_dict.get(find, 1)
            word_set.add(find)

    word_df = pd.DataFrame(
        list(zip(ham_TF_dict.keys(), ham_TF_dict.values(), spam_TF_dict.values(), IDF_dict.values())))
    word_df.columns = ['keyword', 'ham_TF', 'spam_TF', 'IDF']
    word_df['ham_TF'] = word_df['ham_TF'].astype('float') / train_set[train_set['label'] == HAM_LABEL].shape[0]
    word_df['spam_TF'] = word_df['spam_TF'].astype('float') / train_set[train_set['label'] == SPAM_LABEL].shape[0]
    word_df['IDF'] = np.log10(train_set.shape[0] / word_df['IDF'].astype('float'))
    word_df['ham_TFIDF'] = word_df['ham_TF'] * word_df['IDF']
    word_df['spam_TFIDF'] = word_df['spam_TF'] * word_df['IDF']
    word_df['diff'] = word_df['spam_TFIDF'] - word_df['ham_TFIDF']

    selected_spam_key = word_df.sort_values('diff', ascending=False)

    print(
        '>>>Generating Tabu List...\n  Tabu List Size: {}\n  File Name: {}\n  The words shorter than {} are ignored by model\n'.format(
            tabu_size, path, ignore))
    file = open(path, 'w')
    for word in selected_spam_key.head(tabu_size).keyword:
        file.write(word + '\n')
    file.close()


def read_tabu_list(filename):
    file = open(filename, 'r')
    keyword_dict = {}
    i = 0
    for line in file:
        keyword_dict.update({line.strip(): i})
        i += 1
    return keyword_dict


def convert_Content(content, tabu):
    m = len(tabu)
    res = np.int_(np.zeros(m))
    finds = re.findall('[A-Za-z]+', content)
    for find in finds:
        find = find.lower()
        try:
            i = tabu[find]
            res[i] = 1
        except:
            continue
    return res


def learn(train_set, tabu, m):
    n = train_set.shape[0]
    X = np.zeros((n, m))
    Y = np.int_(train_set.label == SPAM_LABEL)
    for i in range(n):
        X[i, :] = convert_Content(train_set.iloc[i].content, tabu)

    NaiveBayes = MultinomialNB()
    NaiveBayes.fit(X, Y)

    Y_hat = NaiveBayes.predict(X)
    accuracy = metrics.accuracy_score(Y, Y_hat)
    print('Accuarcy: {}'.format(accuracy))

    return NaiveBayes


def test(test_set, NaiveBayes, tabu, m):
    n = test_set.shape[0]
    X = np.zeros((n, m))
    Y = np.int_(test_set.label == SPAM_LABEL)
    print(Y)
    for i in range(n):
        X[i, :] = convert_Content(test_set.iloc[i].content, tabu)
    Y_hat = NaiveBayes.predict(X)
    accuracy = metrics.accuracy_score(Y, Y_hat)
    recall = metrics.recall_score(Y,  Y_hat)
    precision = metrics.precision_score(Y,  Y_hat)

    print('Testing Sample Size: {}'.format(n))
    print('Accuarcy: {}'.format(accuracy))
    print('Precision: {}'.format(precision))
    print('Recall: {}'.format(recall))
    print("Confusion matrix:\n%s" %
          metrics.confusion_matrix(Y, Y_hat))


def predictSMS(SMS, NaiveBayes, tabu, m):
    X = convert_Content(SMS, tabu)
    Y_hat = NaiveBayes.predict(X.reshape(1, -1))
    if int(Y_hat) == 1:
        print('SPAM: {}'.format(SMS))
    else:
        print('HAM: {}'.format(SMS))

if __name__ == "__main__":

    tabu_file = '../datasets/tabu.txt'          # user defined tabu file
    tabu_size = 300              # how many features are used to classify spam
    word_len_ignored = 3            # ignore those words shorter than this variable
    # build a tabu list based on the training data
    train_set, test_set = readData()
    generate_tabu_list(tabu_file, train_set, tabu_size, word_len_ignored)

    tabu = read_tabu_list(tabu_file)
    m = len(tabu)
    # train the Naive Bayes Model using training data
    NaiveBayes=learn(train_set, tabu, m)
    # Test Model using testing data
    test(test_set, NaiveBayes, tabu, m)
