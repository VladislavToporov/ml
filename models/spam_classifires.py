#!/usr/bin/env python

"""Train classifiers to predict MNIST data."""

import numpy as np
import time

# Classifiers
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.naive_bayes import GaussianNB, BaseDiscreteNB, MultinomialNB, BernoulliNB
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
from sklearn.neural_network import BernoulliRBM
from sklearn.pipeline import Pipeline
from sklearn.linear_model import LogisticRegression
from sklearn import cross_validation
from sklearn.ensemble import GradientBoostingClassifier
import skflow


# from tensorflow.contrib import skflow


def dropout_model(x, y):
    """DNN with 500, 200 hidden layers, and dropout of 0.5 probability."""
    layers = skflow.ops.dnn(x, [500, 200], keep_prob=0.5)
    return skflow.models.logistic_regression(layers, y)


# Convolutional network, see
# https://github.com/tensorflow/skflow/blob/master/examples/mnist.py
import tensorflow as tf


def max_pool_2x2(tensor_in):
    """
    Max pooling of 2x2 patches with padding at the borders and stride of 2.

    Parameters
    ----------
    tensor_in : tensor

    Returns
    -------
    tensor
    """
    return tf.nn.max_pool(tensor_in,
                          ksize=[1, 2, 2, 1],
                          strides=[1, 2, 2, 1],
                          padding='SAME')


def conv_model(x, y):
    """
    Create a convolutional neural network model.

    Parameters
    ----------
    x : features
    y : labels

    Returns
    -------
    model object
    """
    x = tf.reshape(x, [-1, 28, 28, 1])
    with tf.variable_scope('conv_layer1'):
        h_conv1 = skflow.ops.conv2d(x, n_filters=32, filter_shape=[5, 5],
                                    bias=True, activation=tf.nn.relu)
        h_pool1 = max_pool_2x2(h_conv1)
    with tf.variable_scope('conv_layer2'):
        h_conv2 = skflow.ops.conv2d(h_pool1, n_filters=64, filter_shape=[5, 5],
                                    bias=True, activation=tf.nn.relu)
        h_pool2 = max_pool_2x2(h_conv2)
        h_pool2_flat = tf.reshape(h_pool2, [-1, 7 * 7 * 64])
    h_fc1 = skflow.ops.dnn(h_pool2_flat,
                           [1024],
                           activation=tf.nn.relu,
                           keep_prob=0.5)
    return skflow.models.logistic_regression(h_fc1, y)


def main(C = None, gamma = .0073):
    """Run experiment with multiple classifiers."""
    data = get_data()
    #
    # print("Got %i training samples and %i test samples." %
    #       (len(data['train']['X']), len(data['test']['X'])))

    # Get classifiers
    classifiers = [
        ('Logistic Regression (C=10000)', LogisticRegression(C=10000)),
        ('SVM, adj. C=13', SVC(C=13, gamma = .0073)),
        ('SVM, linear', SVC(kernel="linear", C=0.025)),
        ('k nn', KNeighborsClassifier()),
        ('Decision Tree', DecisionTreeClassifier(max_depth=7)),#max_depth=5
        ('Random Forest', RandomForestClassifier()),
        ('Boosted', GradientBoostingClassifier()),
        ('Gaussian NB', GaussianNB()),
        ('Multinomial NB', MultinomialNB()),
        ('Bernoulli NB', BernoulliNB()),
    ]

    # Fit them all
    classifier_data = {}
    for clf_name, clf in classifiers:
        print("#" * 80)
        print("Start fitting '%s' classifier." % clf_name)
        examples = 100000  # Reduce data to make training faster
        t0 = time.time()
        clf.fit(data['train']['X'][:examples], data['train']['y'][:examples])
        t1 = time.time()
        an_data = analyze(clf, data, t1 - t0, clf_name=clf_name)
        classifier_data[clf_name] = {'training_time': t1 - t0,
                                     'testing_time': an_data['testing_time'],
                                     'accuracy': an_data['accuracy']}



def analyze(clf, data, fit_time, clf_name=''):
    """
    Analyze how well a classifier performs on data.

    Parameters
    ----------
    clf : classifier object
    data : dict
    fit_time : float
    clf_name : str

    Returns
    -------
    dict
        accuracy and testing_time
    """
    results = {}

    # Get confusion matrix
    from sklearn import metrics
    t0 = time.time()
    predicted = np.array([])
    for i in range(0, len(data['test']['X']), 128):  # go in chunks of size 128
        predicted_single = clf.predict(data['test']['X'][i:(i + 128)])
        predicted = np.append(predicted, predicted_single)
    t1 = time.time()
    results['testing_time'] = t1 - t0
    print("Classifier: %s" % clf_name)
    print("Training time: %0.4fs" % fit_time)
    print("Testing time: %0.4fs" % results['testing_time'])
    print("Confusion matrix:\n%s" %
          metrics.confusion_matrix(data['test']['y'],
                                   predicted))
    results['accuracy'] = metrics.accuracy_score(data['test']['y'], predicted)
    results['recall'] = metrics.recall_score(data['test']['y'], predicted)
    results['precision'] = metrics.precision_score(data['test']['y'], predicted)
    print("Accuracy: %0.4f" % results['accuracy'])
    print("Precision: %0.4f" % results['precision'])
    print("Recall: %0.4f" % results['recall'])

    return results

def get_data():
    from sklearn.datasets import load_digits
    from sklearn.utils import shuffle

    file = open("spambase.data").readlines()
    x = []
    y = []
    for line in file:
        elems = line.strip().split(",")
        x.append(list(map(float, elems[:-1])))
        y.append(int(elems[-1]))
    # x = np.array(x).reshape(-1, 1)
    x, y = shuffle(x, y, random_state=0)

    from sklearn.cross_validation import train_test_split
    x_train, x_test, y_train, y_test = train_test_split(x, y,
                                                        test_size=0.33,
                                                        random_state=42)
    data = {'train': {'X': x_train,
                      'y': y_train},
            'test': {'X': x_test,
                     'y': y_test}}


    return data

if __name__ == '__main__':
    main()
