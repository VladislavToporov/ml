from numpy import array
from numpy import asarray
from numpy import zeros
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Flatten
from keras.layers import Embedding
import pandas as pd
import numpy as np


# define documents
def readData(filename):
    email_dataset = pd.read_csv(filename, usecols=[0, 1], encoding='latin-1')[1:]
    email_dataset.columns = ['label', 'content', ]
    # return email_dataset.content, np.int_(email_dataset.label == '1')
    return email_dataset.content, np.int_(email_dataset.label == 'spam') #email_dataset.label



if __name__ == "__main__":
    docs, labels = readData(filename='../datasets/sms_spam.csv')
    t = Tokenizer()
    t.fit_on_texts(docs)
    vocab_size = len(t.word_index) + 1

    encoded_docs = t.texts_to_sequences(docs)
    print(encoded_docs)
    max_length = 4
    padded_docs = pad_sequences(encoded_docs, maxlen=max_length, padding='post')
    print(padded_docs)
    embeddings_index = dict()
    f = open('glove.6B.50d.txt', encoding="UTF-8")
    for line in f:
        values = line.split()
        word = values[0]
        coefs = asarray(values[1:], dtype='float32')
        embeddings_index[word] = coefs
    f.close()
    print('Loaded %s word vectors.' % len(embeddings_index))
    # create a weight matrix for words in training docs
    embedding_matrix = zeros((vocab_size, 50))
    for word, i in t.word_index.items():
        embedding_vector = embeddings_index.get(word)
        if embedding_vector is not None:
            embedding_matrix[i] = embedding_vector
    # define model
    model = Sequential()
    e = Embedding(vocab_size, 50, weights=[embedding_matrix], input_length=4, trainable=False)
    model.add(e)
    model.add(Flatten())
    model.add(Dense(1, activation='sigmoid'))
    # compile the model
    model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['acc'])
    # summarize the model
    print(model.summary())
    # fit the model
    model.fit(padded_docs, labels, epochs=50, verbose=0)
    # evaluate the model
    loss, accuracy = model.evaluate(padded_docs, labels, verbose=0)
    print('Accuracy: %f' % (accuracy * 100))
    from sklearn.metrics import classification_report

    predicted = model.predict_classes(padded_docs)
    report = classification_report(labels, predicted)
    print(report)
